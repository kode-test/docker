FROM ubuntu:latest
RUN apt-get update && apt-get -y install cron

# Copy 1-cron file to the cron.d directory
COPY 1-cron /etc/cron.d/hello-cron
 
# Give execution rights on the cron job
RUN chmod 0644 /etc/cron.d/hello-cron

# Apply cron job
RUN crontab /etc/cron.d/hello-cron
 
# Create the log file to be able to run tail
RUN touch /var/log/test.log
 
# Run the command on container startup
CMD ["cron", "-f"]
